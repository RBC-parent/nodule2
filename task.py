import utime
from time import sleep
from json import dumps

# TODO from machine import ADC, I2C, SPI, pin
# import onewire
# import dht
# https://docs.micropython.org/en/latest/esp32/quickref.html#installing-micropython
# from machine import RTC
# rtc = RTC()
# rtc.datetime((2017, 8, 23, 1, 12, 48, 0, 0)) # set a specific date and time

start_time = int(utime.time())
with open('./noduleid.txt', 'r') as nodule_id_file:
    nodule_id = nodule_id_file.read().strip()


class SensorParent(object):
    def __init__(self, pin_number):
        super(SensorParent, self).__init__()
        self.pin_number = pin_number

    def is_task(self):
        return

    def read(self, args):
        # raise(Exception("WTF?!"))
        print("default")
        return {"result": {"result": "default"}}


class stats(SensorParent):
    def __init__(self, pin_number):
        super(stats, self).__init__(pin_number)

    def read(self, args):
        print("Returning stats about nodule")
        return {"result": {"nodule_id": nodule_id}}


class ds18b20(SensorParent):
    def __init__(self, pin_number):
        super(ds18b20, self).__init__(pin_number)

    def read(self, args):
        print("Reading temp of ds18b20 at pin {}".format(self.pin_number))
        import urandom
        result = urandom.getrandbits(4)
        return {"result": {"measurement": result}}


class moisture(SensorParent):
    def __init__(self, pin_number):
        super(moisture, self).__init__(pin_number)

    def read(self, args):
        print("Reading moisture of DHT_11 at pin {}".format(self.pin_number))
        import urandom
        result = urandom.getrandbits(4)
        return {"result": {"measurement": result}}


class DHT_11(SensorParent):
    def __init__(self, pin_number):
        super(DHT_11, self).__init__(pin_number)

    def read(self, args):
        print("Reading temp of DHT_11 at pin {}".format(self.pin_number))
        import urandom
        result = urandom.getrandbits(4)
        return {"result": {"measurement": result}}


class TSL2561(SensorParent):
    def __init__(self, pin_number):
        super(TSL2561, self).__init__(pin_number)

    def read(self, args):
        print("Reading temp of TSL2561 at pin {}".format(self.pin_number))
        import urandom
        result = urandom.getrandbits(4)
        return {"result": {"measurement": result}}
    def do_it(self, args):
        print("Reading temp of TSL2561 at pin {}".format(self.pin_number))
        import urandom
        result = urandom.getrandbits(4)
        return {"result": {"measurement": result}}


class IP(SensorParent):
    def __init__(self, pin_number):
        super(IP, self).__init__(pin_number)

    def read(self, args):
        print("reading ")
        return {"result": {"address": "0.0.0.0"}}


class uptime(SensorParent):
    def __init__(self, pin_number):
        super(uptime, self).__init__(pin_number)

    def read(self, args):
        print("actuating ")
        now = int(utime.time())  # TODO convert to time string
        diff = now - start_time
        seconds = diff
        minutes = seconds // 60
        hours = minutes // 60
        days = hours // 24
        months = days // 30
        years = months // 12
        uptime = "{}-{}-{}T{}:{}:{}z".format(zfill(years, 4), zfill(months, 2), zfill(days, 2), zfill(hours, 2), zfill(minutes, 2), zfill(seconds, 2))
        return {"result": {"uptime": str(uptime)}}


def zfill(s, l):
    s = str(s)
    diff = l - len(s)
    if diff <= 0:
        return s
    pad = '0' * diff
    return pad + s


class servo(SensorParent):
    def __init__(self, pin_number):
        super(servo, self).__init__(pin_number)

    def read(self, args):
        print("actuating ")
        return {"result": "actuating"}


class pump(SensorParent):
    def __init__(self, pin_number):
        super(pump, self).__init__(pin_number)

    def read(self, args):
        print("actuating ")
        return {"result": "actuating"}


g = globals().copy()
task_lookup = {name: obj for name, obj in g.items() if 'is_task' in dir(obj)}
print("Found the following tasks/components:")
print(task_lookup)


class Dispatcher(object):
    """docstring for Dispatcher."""

    def __init__(self, task_id, run_id, kind, pin, action=None, args=None):
        super(Dispatcher, self).__init__()
        # print(task_id)
        self.task_id = task_id
        self.run_id = run_id
        self.pin = pin
        self.kind = task_lookup.get(kind, SensorParent)  # TODO if lookup fails, go straight to reporting error
        self.action = action  # TODO how to lookup different methods on hardware types?
        print(kind, self.kind, action)
        self.args = args
        self.instance = self.kind(pin)

    def run(self):
        func = getattr(self.instance, self.action)
        sleep(0.1)
        try:
            results = func(self.args)
        except Exception as e:
            results = {'error': e}
        default = {'task_id': self.task_id, 'run_id': self.run_id, "action": self.action,
                   "pin": self.pin, "nodule_id": nodule_id, "kind":  str(self.kind).replace('<class ', '').replace('>', '')[1:-1]}
        default.update(results)
        return dumps(default)
