# Start up
# Get config from hardcoded DNS location
# Listen to MQTT
# Callback adds job to internal queue
# Every x seconds (configurable), check queue and pop next job
# In each job, create an instance of the job type and associated hardware port
# Perform the specified action - read or write port - and record results
# Return results tagged by original job ID via MQTT

from time import sleep
from task import Dispatcher
from task_queue import TaskQueue
from json import loads, dumps
from sys import argv
from umqtt.robust import MQTTClient
from os import environ

nodule_id = 'xxx999'
with open('./noduleid.txt', 'r') as nodule_id_file:
    nodule_id = nodule_id_file.read().strip()
if len(argv) > 1:
    nodule_id = argv[1]

job_topic = 'jobs/'+nodule_id
results_topic = 'results/'+nodule_id
errors_topic = 'errors/'+nodule_id
presence_topic = 'presence/'+nodule_id
time_topic = 'time'
# broker = "0.0.0.0"
broker = "mqtt"
mqtt_host = environ.get('broker', "mqtt")
mqtt_host = environ.get('nodule_id', "def456")

q = TaskQueue(num_workers=1)
print("FOOOO!")

def publish_results(client):
    def _publish_results(results):
        print("Returning results...")
        res = loads(results)
        print(results)
        try:
            key = list(res.get('result').keys())[0]
        except:
            key = "foo"
        topic = results_topic + "/" + res.get('kind', 'unknown') + '/' + key
        results = res.get('result').get(key, res)
        print(topic, results)
        # topic = errors_topic if 'error' in results else results_topic
        client.publish(topic, str(results).replace("'", '"'))
        print(str(results).replace("'", '"'))
    return _publish_results


def on_message2(client):
    def _on_message2(userdata, message):
        return on_message(client, message)
    return _on_message2


def on_message(client):
    def _on_message(topic, message):
        msg_json = loads(message.decode("utf-8"))
        print(str(topic))
        if str(topic) is "b'time'":
            print(msg_json.get('time'))
            return
        if msg_json.get('stop'):
            q.stop()
            print("Killing queue, stopping")
            return
        task = Dispatcher(**msg_json)
        q.add_task(task.run, publish_callback={'func': publish_results(client),
                                               'client': client})
    return _on_message


def prepare_client(nodule_id, broker):
    client = None
    print("Nodule "+nodule_id+" connecting to broker at "+broker)
    client = MQTTClient(nodule_id, broker, port=1883)
    sleep(1)
    client.connect()
    client.set_callback(on_message(client))
    client.check_msg()
    print("Connected")
    return client


client = prepare_client(nodule_id, broker)
client.publish(presence_topic, dumps({"presence": "Connected", "node": nodule_id}))
client.subscribe(job_topic)
client.subscribe(time_topic)
while q.run:
    q.check()
    client.check_msg()
    sleep(0.5)
q.join()
print("Disconnecting from broker")
client.disconnect()
client.loop_stop()
