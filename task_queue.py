from collections.deque import deque


class TaskQueue(deque):
    def __init__(self, num_workers=1):
        self.q = deque()
        self.run = True

    def stop(self):
        self.run = False

    def add_task(self, task, publish_callback, *args, **kwargs):
        args = args or ()
        kwargs = kwargs or {}
        self.q.append((task, publish_callback, args, kwargs))

    def check(self):
        if len(self.q) > 0:
            item, publish_callback, args, kwargs = self.q.popleft()
            results = item(*args, **kwargs)
            # print(results)
            publisher = publish_callback.get('func')
            client = publish_callback.get('client')
            publisher(results)
