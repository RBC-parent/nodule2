FROM registry.gitlab.com/rbc-parent/micropython:latest

COPY . /scripts

WORKDIR /scripts

CMD ["micropython", "./main.py"]

# TODO add environment variables
